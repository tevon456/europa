import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class Tab extends PureComponent {
  handleClick = () => {
    this.props.handleClick(this.props.tabIndex);
  };

  render() {
    return (
      <li
        className={`Tabs__item ${
          this.props.selected ? "Tabs__item--selected" : ""
        }`}
        onClick={this.handleClick}
      >
        <p style={{fontWeight:"500"}}>{this.props.title}</p>
      </li>
    );
  }
}

Tab.propTypes = {
  title: PropTypes.string.isRequired,
  handleClick: PropTypes.func,
  tabIndex: PropTypes.number,
  selected: PropTypes.bool
};

export default Tab;
