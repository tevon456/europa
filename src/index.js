import React from 'react';
import { render } from 'react-dom';
import { Logo, Heading } from 'chramework';
import Tabs, { Tab } from './Tabs';
import POLI from './frames/poli'
import BIOP from './frames/biop';
import 'chramework/dist/main.css';
import '../src/styles/style.css';
import '../src/styles/variables.css';
import POLH from './frames/polh';



class App extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
		  policy: '',
		  client:''
		};
}

onChangePolicy(e) {
    this.setState({ policy: e.target.value });
}

onChangeClientId(e) {
    this.setState({ client: e.target.value });
}
	  render() {
		return (
	<div className="App">
		<Tabs>
		    <Tab title="Home">  
				<p>Note its best to place these values here first or errors might get thrown</p>
				<input onChange={this.onChangePolicy.bind(this)} type="text" placeholder="Policy eg 002123456" /><br />
				<input onChange={this.onChangeClientId.bind(this)} type="text" placeholder="Client id" /><br />
				<br />
				<p>Policy using:<span style={{color:"var(--accent)"}}>{this.state.policy}</span></p>
				<p>Client ID using:<span style={{color:"var(--accent)"}}>{this.state.client}</span></p>
			</Tab>
			<Tab title="POLI">
				<POLI data={this.state} />
			</Tab>

			<Tab title="POLH">
				<POLH data={this.state}/>
			</Tab>
			
			<Tab title="BIOP">
			  <BIOP data={this.state}/>
			</Tab>

			<Tab title="FSUR">
			   <p>in development</p>
			</Tab>

			<Tab title="ASUR">
			   <p>in development</p>
			</Tab>

			<Tab title="GRPA">
			   <p>in development</p>
			</Tab>
			
		</Tabs>
	</div>
   );
}
}
export default App
render(<App />, document.getElementById('root'));
