import React from "react";

class POLH extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            screen: 'Z',
            client: '0020000000',
            timeout: 3000,
            enter: 'I'
        };
    }


    componentDidMount() {
        const dde = window.require('electron-node-dde')
        let client = dde.createClient('ptw', 'psl');
        var timeout = 10000;
        client.connect();
        client.execute("cursor 02 015", timeout)
        client.execute("send polh", timeout)
        client.execute("send <Enter>", timeout)
    };

    freshState = () => {
        const dde = window.require('electron-node-dde')
        let client = dde.createClient('ptw', 'psl');
        var timeout = this.state.timeout;
        client.connect();
        client.execute("send <Reset>", timeout)
        console.log("fresh");
    }

    handleSubmit = () => {
        const dde = window.require('electron-node-dde')
        let client = dde.createClient('ptw', 'psl');
        var timeout = this.state.timeout;
        client.connect();
        client.execute("cursor 05 013", timeout)
        client.execute("send " + this.state.client + "", timeout)
        client.execute("cursor 06 010", timeout)
        client.execute("send " + this.state.enter + "", timeout)
        client.execute("cursor 07 010", timeout)
        client.execute("send " + this.state.screen + "", timeout)
        client.execute("send <Enter>", timeout)
    };
    render() {
        return (

            <div style={{ border: "2px solid grey", width: "50%" }}>

                {/* <button type="button" onClick={this.handleSubmit.bind(this)}>Submit</button> */}
                {/* <button type="button" onClick={this.freshState.bind(this)}>fresh state</button><br /> */}
                <p>you'll be brought to the polh screen thats all for now</p>
            </div>
        );
    }
}

export default POLH;
