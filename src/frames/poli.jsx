import React from "react";

class POLI extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      option: 'Z',
      policy: this.props.data.policy,
      timeout: 3000,
      out: ''
    };
  }

  onChangePolicy(e) {
    this.setState({ policy: e.target.value });
  }
  onChangeOption(e) {
    this.setState({ option: e.target.value });
  }

  freshState = () => {
    const dde = window.require('electron-node-dde')
    let client = dde.createClient('ptw', 'psl');
    var timeout = this.state.timeout;
    client.connect();
    client.execute("cursor 06 007", timeout)
    client.execute("send <Reset>", timeout)
    console.log("fresh");
  }

  outPut = () => {
    var path = require('path');
    const fs = require('fs');
    var p = path.basename('C:\\myfile.html');
    // var p = "prometheus.txt"
    fs.readFile(p, 'utf8', function (err, data) {
      if (err) return console.log(err);
      this.setState({ out: data });
    });
  }

  componentDidMount(){
		const dde = window.require('electron-node-dde')
		let client = dde.createClient('ptw', 'psl');
		var timeout = 3000;
		client.connect();
        client.execute("cursor 02 015", timeout)
        client.execute("send poli", timeout)
		client.execute("send <Enter>", timeout)
 };

  handleSubmit = () => {
    const dde = window.require('electron-node-dde')
    let client = dde.createClient('ptw', 'psl');
    var timeout = this.state.timeout;
    client.connect();
    client.execute("cursor 06 007", timeout)
    client.execute("send " + this.state.policy + "", timeout)
    client.execute("cursor 07 009", timeout)
    client.execute("send " + this.state.option + "", timeout)
    client.execute("send <Enter>", timeout)
    // client.execute("message [screen 6 002 24 087];", timeout )

  };

  render() {
    return (

      <div style={{ border: "2px solid grey", width: "50%" }}>
        
          <label>Policy</label><br />
          <input onChange={this.onChangePolicy.bind(this)} type="text" placeholder="Policy eg 002123456" /><br />

          <label>SECTION</label><br />
          <select onChange={this.onChangeOption.bind(this)} type="select">

            <option>A</option>
            <option>B</option>
            <option>C</option>
            <option>D</option>
            <option>E</option>
            <option>F</option>
            <option>G</option>
            <option>H</option>
            <option>I</option>
            <option>J</option>
            <option>K</option>
            <option>L</option>
            <option>M</option>
            <option>N</option>
            <option>O</option>
            <option>P</option>
            <option>Z</option>
          </select><br />

          <button type="button" onClick={this.handleSubmit.bind(this)}>Submit</button>
          <button type="button" onClick={this.freshState.bind(this)}>fresh state</button>
          <button type="button" onClick={this.outPut.bind(this)}>Read</button>
        
        <p>{this.state.option}</p>
        <p>{this.state.policy}</p>
        <p>{this.state.out}</p>
      </div>
    );
  }
}


export default POLI;
