import React from "react";

class BIOP extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            screen: 'Z',
            client: this.props.data.client ,
            timeout: 3000,
            enter: 'I'
        };
    }
    onChangeScreen(e) {
        this.setState({ screen: e.target.value });
    }
    onChangeClient(e) {
        this.setState({ client: e.target.value });
    }
    onChangeEnter(e) {
        this.setState({ enter: e.target.value });
    }

    componentDidMount() {
        const dde = window.require('electron-node-dde')
        let client = dde.createClient('ptw', 'psl');
        var timeout = 10000;
        client.connect();
        client.execute("cursor 02 015", timeout)
        client.execute("send biop", timeout)
        client.execute("send <Enter>", timeout)
    };

    freshState = () => {
        const dde = window.require('electron-node-dde')
        let client = dde.createClient('ptw', 'psl');
        var timeout = this.state.timeout;
        client.connect();
        client.execute("send <Reset>", timeout)
        console.log("fresh");
    }

    handleSubmit = () => {
        const dde = window.require('electron-node-dde')
        let client = dde.createClient('ptw', 'psl');
        var timeout = this.state.timeout;
        client.connect();
        client.execute("cursor 05 013", timeout)
        client.execute("send " + this.state.client + "", timeout)
        client.execute("cursor 06 010", timeout)
        client.execute("send " + this.state.enter + "", timeout)
        client.execute("cursor 07 010", timeout)
        client.execute("send " + this.state.screen + "", timeout)
        client.execute("send <Enter>", timeout)
    };
    render() {
        return (

            <div style={{ border: "2px solid grey", width: "50%" }}>
                <label>Client ID</label>
                <input onChange={this.onChangeClient.bind(this)} type="text" placeholder="" /><br/>
                
                <label>Enter</label>
                <select onChange={this.onChangeEnter.bind(this)} type="select">
                    <option value="I">I: Inquire</option>
                    <option value="C">C: Create</option>
                    <option value="A">A: Assign</option>
                    <option value="M">M: Maintain</option>
                    <option style ={{color:"red"}} value="D">D: Delete</option>
                    <option value="S">S: Search</option>
                    <option value="T">T: Transfer</option>
                    <option style ={{color:"red"}} value="R">R: Reset</option>
                </select><br />

                <label>Screen</label>
                <select onChange={this.onChangeScreen.bind(this)} type="select">
                    <option value="Z">Z: Menu</option>
                    <option value="A">A: Basic info Name</option>
                    <option value="B">B: Basic info Address</option>
                    <option value="C">C: Basic info Contact</option>
                    <option value="D">D: Client Prefrences</option>
                    <option value="K">K: Banking info</option>
                    <option value="O">O: Other info</option>
                    <option value="P">P: Previous info</option>
                    <option value="U">U: U/W, Date, Comments/Reinsurance</option>
                    <option value="L">L: Client Search List</option>
                    <option value="S">S: SSN-TRN / ECRIS list</option>
                    <option value="R">R: US Residency info</option>
                </select><br />

                <button type="button" onClick={this.handleSubmit.bind(this)}>Submit</button>
                <button type="button" onClick={this.freshState.bind(this)}>fresh state</button><br />

                <p>{this.state.client}</p>
                <p>{this.state.enter}</p>
                <p>{this.state.screen}</p>
            </div>
        );
    }
}

export default BIOP;
